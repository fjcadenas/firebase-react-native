import React from "react";
import { StyleSheet, Text, View, Button } from "react-native";

import firebase from './config/firebase/Firebase';

export default function App() {
  const _onClick = () => {
    console.log('Have pressed!');
    firebase.getMapCells();
  }
  return (
    <View style={styles.container}>
      <Text>Hola Irene! cosas en el movil</Text>
      <View style={styles.buttonContainer}>
        <Button title="Press me!" onPress={_onClick} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  },
  buttonContainer: {
    margin: 20
  }
});
