import * as firebase from 'firebase';
import 'firebase/firestore';

import firebaseConfig from './firebaseConfig';

// Initialize firbease
firebase.initializeApp(firebaseConfig);

// Collections
const MAP_CELLS_COLLECTION = 'map-cells';

const Firebase = {
  // get al map cells
  getMapCells: async () => {
    const collection = firebase.firestore().collection(MAP_CELLS_COLLECTION);
    const snapshot = await collection.get();
    if (snapshot.empty) {
      console.log('No documents found!');
      return;
    }
    snapshot.forEach(document => {
      console.log(JSON.stringify(document.data()));
    });
  }
}

export default Firebase;